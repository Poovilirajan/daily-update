var server= require('WS').Server;
var s=new server({port:5001});
s.on('connection',function(ws){
    //console.log('connected!');
    ws.on('message',function(ms){
        s.clients.forEach(function e(cl){
            if(cl!=ws)
                cl.send(ms); 
        });
        //console.log(ms);
    });
    ws.on('close',function(){
        console.log('A client Disconnected');
    });
});