//Javascript classes
//class-like structures in ES5
function classLikeStructuresInEs5(){
    function Person(){
        this.name;
    }
    Person.prototype.setName=function(name){
        this.name=name;
    }
    Person.prototype.getName=function(){
        return this.name;
    }
    let person1=new Person();
    person1.setName('Steve');
    console.log(person1.getName());
    console.log(typeof person1);
    console.log(person1 instanceof Person);
}
function classDeclaration(){
    //basic class declaration
    class Person{
        constructor(name){
            this.name=name;
        }
        setName(name){
            this.name=name;
        }
        getName(){
            return this.name;
        }
    }
    let person1=new Person('Steve');
    let person2=new Person();
    person2.setName('Rogers');
    console.log(person1.getName());
    console.log(person2.getName());
    console.log(typeof Person);
    console.log(typeof person1);
    //constant class names
    class Sample{
        constructor(){
            //Sample='Samp';//error because class name inside class is constant 
        }
    }
    let s=new Sample();
}
//class Expressions
function classExpressions(){
    //basic class Expression
    let Person=class{
        constructor(name){
            this.name=name;
        }
        setName(name){
            this.name=name;
        }
        getName(){
            return this.name;
        }
    }
    let person1=new Person('Tony');
    let person2=new Person();
    person2.setName('Stark');
    console.log(person1.getName());
    console.log(person2.getName());
    console.log(typeof Person);
    console.log(typeof person1);
    //Named class expressions
    let Demo=class Sample{
        constructor(){
            this.type=typeof Sample;
        }
        getType(){
            return this.type;
        }
    }
    let samp=new Demo();
    console.log(samp.getType());//value of sample is retrieved from the class 
    console.log(typeof Demo);
    console.log(typeof Sample);//undefined because Sample is only available inside the Demo class
}
//Classes as First-Class Citizens
function classesAsFirstClassCitizens(){
    //Passing class as function Arguments
    function createObjFromClass(className){
        return new className();
    }
    class Sample{
        getNumber(){
            return (Math.random()*10).toFixed();
        }
    }
    let num1=createObjFromClass(Sample);
    console.log(num1.getNumber());
    //passing anonymous class as function arguments
    let num2=createObjFromClass(class{
        getNumber(){
            return (Math.random()*10).toFixed();
        }
    });
    console.log(num2.getNumber());
    //Creating singletons using new and class Expression
    let num3=new class{
        constructor(num){
            this.num=num;
        }
        isOddOrEven(){
            if(this.num%2===0)
                console.log(this.num,'is Even');
            else
                console.log(this.num,'is odd');
        }
    }((Math.random()*10).toFixed());
    num3.isOddOrEven();
}
//Computed method names and accessor properties
function accessorPropertiesAndComputedMethodNames(){
    let name='getNum';
    class DemNumber{
        [name](){
            return (Math.random()*10).toFixed();
        }
    }
    let num1=new DemNumber();
    console.log(num1.getNum());
    //accessor properties with computed method names
    let property='names';
    class Person{
        constructor(){
            this.name='Stark';
        }
        get [property](){
            return this.name;
        }
        set [property](nName){
            this.name=nName;
        }
    }
    let p1=Object.getOwnPropertyDescriptor(Person.prototype,'names');
    console.log('set' in p1);
}
function generatorMethodsAndStaticMembers(){
    class userMap{
        constructor(){
            this.data=new Map();
        }
        *[Symbol.iterator](){
            yield *this.data.entries();//delegating generator using default iterator of Map
        }
    }
    let map1=new userMap();
    map1.data.set('name','Tony');
    map1.data.set('age',22);
    for(let v of map1)
        console.log(v);
    let Person=class{
        constructor(name){
            this.name=name;
        }
        getName(){
            return this.name;
        }
        static show(){
            console.log('Class Name: ',Person.name);
        }
    }
    let person1=Person.show();
}
classLikeStructuresInEs5();
classDeclaration();
classExpressions();
classesAsFirstClassCitizens();
accessorPropertiesAndComputedMethodNames();
generatorMethodsAndStaticMembers();