//Sets in ES6
function setsInEs6(){
    let simpleSet=new Set();
    simpleSet.add(7);
    simpleSet.add('7');//number 7 and string '7' are different values 
    console.log('Size of set:',simpleSet.size);
    console.log(simpleSet);
    let obj1={name:'Wayne'};
    let obj2={name:'Stark'};
    //add multiple objects to set
    simpleSet.add(obj1);
    simpleSet.add(obj2);
    simpleSet.add(obj2);//duplicate values are ignored
    console.log('Size of set:',simpleSet.size);
    console.log(simpleSet);
    //initialize set using array
    let array1=[0,1,2,3,4,5,3,1,5,6,7];
    let arraySet=new Set(array1);
    console.log('Size of set:',arraySet.size);
    console.log(arraySet);
    //test values is available or not
    console.log(simpleSet.has(obj1));
    console.log(simpleSet.has(6));//false because 6 not avaialble in set
    simpleSet.delete(obj2);//remove singl value
    console.log('Size of set:',simpleSet.size);
    console.log(simpleSet);
    arraySet.clear();//remove all elements from a set
    console.log('Size of set:',arraySet.size);
    console.log(arraySet);
    //foreach method
    arraySet=new Set(array1);
    arraySet.forEach(function(v1,v2,v3){//callback function have 3 parameters 
        console.log(v1);
        console.log(v3);
    });
    //convert set into array
    let array2=[...simpleSet,...arraySet];
    console.log(array2);
    //generate random unique n number from given range
    function genRandom(start,end,count){
        let tempSet=new Set();
        while(tempSet.size!=count){
            tempSet.add((Math.random()*(end-start)+start).toFixed());
        }
        return [...tempSet];
    }
    let numbersbetween20_50=genRandom(20,50,15);
    console.log(numbersbetween20_50);
}
//Weaksets
function weakSets(){
    //creating weak sets
    let wset=new WeakSet();
    let obj1={id:1};
    wset.add(obj1);//it only accepts objects
    //wset.add(7);//throws error it doesn't accepts primitive types
    let obj2={id:2};
    wset.add(obj2);
    console.log(wset);
    obj2=null;
    console.log(wset);
    //console.log(wset.has(obj2));//false because the obj2 reference was removed but the value is still available in the set but it is inaccessible
}
function mapsInEs6(){
    //creating map
    let simpleMap=new Map();
    //adding items
    simpleMap.set('name','Steve');
    //retrieve items
    console.log('Name : ',simpleMap.get('name'));
    console.log(simpleMap.get('age'));//undefined because key(age) is not available in map
    //objects as keys in map
    let obj1={id:1},obj2={id:2};
    simpleMap.set(obj1,obj1.id);
    simpleMap.set(obj2,obj2.id);
    console.log('Object1: ',simpleMap.get(obj1));
    //map methods
    //has method
    console.log(simpleMap.has(obj2));//true the key is available
    //delete method
    simpleMap.delete(obj2);
    console.log(simpleMap.has(obj2))//false the key is not available 
    //size property
    console.log('Size of map: ',simpleMap.size);
    console.log(simpleMap);
    //clear method
    simpleMap.clear();
    console.log('Size of map: ',simpleMap.size);//0 because all the key-value pairs are removed from the map
    //initializes map with an array
    let name=['name','Wayne'];
    let age=['age',21];
    let arrayMap=new Map([name,age]);
    console.log(arrayMap);
    //foreach method on map
    arrayMap.forEach((v1,v2,v3)=>{
        console.log('key: ',v2);
        console.log('value: ',v1);
        console.log('map: ',v3);
    });
}
setsInEs6();
weakSets();
mapsInEs6();