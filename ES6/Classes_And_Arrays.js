//Inheritance in Es6
class Dog1{
    constructor(){
        this.bark='wow wow';
    }
    barking(){
        console.log(this.bark);
    }
}
class BabyDog1 extends Dog1{
    constructor(){
        super();
    }
}
let puppy1=new BabyDog1();
puppy1.barking();
console.log(puppy1 instanceof Dog1);
console.log(puppy1 instanceof BabyDog1);
//derived class without constructor
class Babydog2 extends Dog1{
    //here super method is called automatically
}
let puppy2=new Babydog2();
puppy2.barking();
//shadowing class Methods
class Dog2{
    constructor(sound){
        this.sound=sound;
    }
    barking(){
        console.log(this.sound);
    }
}
class BabyDog3 extends Dog2 {
    barking(){//shadowing/overriding method of baseclass 
        console.log(this.sound);
    }
}
let puppy3=new BabyDog3('bow bow');
puppy3.barking();
class BabyDog4 extends Dog2 {
    barking(){//shadowing/overriding method of baseclass using super 
        super.barking();
    }
}
let puppy4=new BabyDog4('bow bow');
puppy4.barking();
//inherited static members
class Cat{
    constructor(sound){
        this.sound=sound;
    }
    makeSound(){
        console.log(this.sound);
    }
    static make(sound){
        return new Cat(sound);
    }
}
class BabyCat extends Cat{

}
let cat1=BabyCat.make('meow meow');//static method also available in Derived calss
cat1.makeSound();
console.log(cat1 instanceof Cat);
console.log(cat1 instanceof BabyCat);//false because even cat1 was created by BabyCat class but cat1 holds the instance of Cat
//Derived class From an expressions
class BabyDog5 extends Dog{//Dog is an Custom Type from Es5  
    constructor(){
        super();
    }
}
let puppy5=new BabyDog5();
puppy5.barking();
//Dynamiclly determine Base class
function getBaseClass(){
    return Dog;
}
class BabyDog6 extends getBaseClass(){//the base class determined by the function 

}
let puppy6=new BabyDog6();
puppy6.barking();
//Inheriting from Built-ins
class DemArray extends Array{
}
let odds=new DemArray();
odds.push('5');
console.log(odds[0]);
console.log(odds.length);
odds.length=0;
console.log(odds[0]);//undefined because of built-in behavior of array
//Symbol.species property
let nums1=new DemArray(1,2,3,4,5);
let nums2=nums1.slice(3,5);
console.log(nums2);
console.log(nums1 instanceof DemArray);
console.log(nums2 instanceof DemArray);//it also true because of symbol.species property 
//userdefine symbol.species
class Demo{
    static get [Symbol.species](){
        return this;
    }
    constructor(v){
        this.value=v;
    }
    copy(){
        return new this.constructor[Symbol.species](this.value);
    }
}
class DemoDerived extends Demo{

}
let obj=new DemoDerived('Stark');
let obj2=obj.copy();
console.log(obj instanceof DemoDerived);
console.log(obj2 instanceof DemoDerived);
console.log(obj2.value);
}
function usingNewTargetInClassConstructors(){
class Animal{
    constructor(sound){
        console.log(new.target==Animal);
        this.sound=sound;
    }
}
let dog=new Animal('wow wow');
//new.target is not same always 
class Baby extends Animal{
    constructor(sound){
        super(sound);
    }
}
let puppy=new Baby('bow bow');//false because new.target != animal
//abstract class using new.target
class Person{
    constructor(){
        if(new.target===Person)
            throw new Error('class cannot be instantiated directly');
    }
}
class Male extends Person{
    constructor(name){
        super();
        this.name='Mr.'+name;
    }
}
//let person1=new Person();//throws error
let person2=new Male('Tony');
console.log(person2 instanceof Person);
}
//types of inheritance
function typesOfInheritance(){
//classical inheritance
class vehicle{
    constructor(name){
        this.name=name;
    }
    display(){
        console.log('Name: ',this.name);
    }
}
class Bike extends vehicle{
    constructor(name,brand,wheels){
        super(name);
        this.brand=brand;
        this.wheels=wheels;
    }
    display(){
        super.display();
        console.log('Brand: ',this.brand);
        console.log('Wheels: ',this.wheels);
    }
}
let Mt15=new Bike('Mt15','YAMAHA',2);
Mt15.display();
//prototypal inheritance
let Length1={
    getLength(){
        return this.sound.length;
    }
};
let Makesound={
    makesound(){
        console.log(this.sound);
    }
};
function MixedFun(vals){
    var obj=function(){};
    Object.assign(obj.prototype,vals);//prototypal inheritance
    return obj;
}
class Baby extends MixedFun(Makesound){
    constructor(sound){
        super();
        this.sound=sound;
    }
}
let kitten1=new Baby('meew meew');
kitten1.makesound();
let puppy7=new Baby('bow bow');
puppy7.makesound();
}
//Improved Array Capabilities
//creatingArrays
function creatingArrays(){
    //creating array with new
    let array1=new Array(5);//creates an empty array with length 5
    console.log(array1.length);
    console.log(array1[0]);//undefined because array is empty
    array1=new Array('3')//creates an array with single value 3 at index 0
    console.log(array1.length);
    console.log(array1[0]);
    array1=new Array(7,'1');//creates an array with values number7, character '1'
    console.log(array1.length);
    console.log(array1[0]);
    console.log(array1[1]);
    array1=new Array(5,9);//creates an array with values 5,9
    console.log(array1.length);
    console.log(array1[0]);
    console.log(array1[1]);
    //creating array with Array.of()
    array1=Array.of(3,7);//creates an array with values 1,2
    console.log(array1.length);
    console.log(array1[0]);
    console.log(array1[1]);
    array1=Array.of('5');
    console.log(array1.length);
    console.log(array1[0]);//creates an array with value character '5'
    //covert array-like objects into array using Array.from()
    let set1=new Set([1,3,3,3,5,7,9]);
    array1=Array.from(set1);
    console.log(array1);
    let map1=new Map([['name','Steve'],['age',70]]);
    array1=Array.from(map1);
    console.log(array1);
    //mapping conversion
    let birthyear=[1978,1995,1997,1999,2000];
    let age=Array.from(birthyear,(v)=>2021-v);
    console.log(age);
    let FindAge={
        currentyear:2021,
        find(year){
            return this.currentyear-year;
        }
    };
    let age1=Array.from(birthyear,FindAge.find,FindAge);//third arguments is this value for mapping function
    console.log(age1);
    //Use on iterables
    let NamesObj={
        names:['Steve','Wayne','Stark'],
        *[Symbol.iterator](){
            yield *this.names.values();
        }
    }
    array1=Array.from(NamesObj);
    console.log(array1);
}
//New methods on all array
function newMethodsOnAllArray(){
    //find & findIndex methods
    let numbers=[1,3,5,7,9,0];
    //find even number in the array
    console.log(numbers);
    console.log('Even number in the array ',numbers.find((v)=>v%2==0));
    console.log('Even number found in', numbers.findIndex((v)=>v%2==0)+1, 'position');
    //fill()method
    numbers.fill(1);//overwrites all elements with value 1
    console.log(numbers);
    numbers=[1,3,5,7,9,0];
    numbers.fill(1,3);//overwrites with value 1 from index 3
    console.log(numbers);
    numbers=[1,3,5,7,9,0];
    numbers.fill(7,2,5);//overwrite the elements from index 2 to index 4 with value 7
    console.log(numbers);
    //copywithin method
    numbers=[1,3,5,7,9,0];
    numbers.copyWithin(4,0)
    console.log(numbers);
    numbers=[1,2,3,5,7,9,0];
    numbers.copyWithin(3,0,2)
    console.log(numbers);
}
inheritanceWithDerivedClass();
usingNewTargetInClassConstructors();
typesOfInheritance();
creatingArrays();
newMethodsOnAllArray();