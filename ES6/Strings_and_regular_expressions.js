//codePointAt()
            let txt='S';
            console.log(txt.codePointAt(0));//prints unicodcode point
            //String.fromCodePoint()
            console.log('83 represents :',String.fromCodePoint(83));
            console.log('32-bit character :',String.fromCodePoint(134071));
            //normalize()
            let names=['Stark','Wayne','Bruce','Rogers','Clint'];
            let normalizedNames=names.map(name=> name.normalize('NFKD'));
            console.log(normalizedNames);
            normalizedNames.sort((name1,name2)=>{
                if(name1<name2)
                   return -1;
                else if(name1===name2)
                    return 0;
                else
                    return 1;
            });
            console.log(normalizedNames);
            txt=String.fromCodePoint(134071);//32-bit code unit character
            console.log(/^.$/.test(txt));//false beacause regex applied on 16-bit code units(single character)
            console.log(/^.$/u.test(txt));//true u flag switch regex to apply on character
            //u flag in regex Constructor
            let pat=new RegExp('.','u');
            console.log(pat.test(txt));
            //methods for identifying substring
            //includes()
            txt='Wakanda Forever!';
            console.log(txt.includes('k'));
            console.log(txt.includes('ever'));
            console.log(txt.includes('k',4));//starts the match from 2nd argument
            //startsWith()
            console.log(txt.startsWith('a'));
            console.log(txt.startsWith('Wakanda'));
            console.log(txt.includes('For',8));//starts the match from 2nd argument
            //endsWith()
            console.log(txt.endsWith('ever!'));
            console.log(txt.endsWith('e'));
            console.log(txt.endsWith('o',10));//match with 2nd argument
            //repeat method
            txt='snap ';
            console.log(txt.repeat(2));
            txt='hi1 hi2 hi3';
            pat=/hi\d\s?/;
            gpat=/hi\d\s?/g;
            stpat=/hi\d\s?/y;
            res=pat.exec(txt);
            gres=gpat.exec(txt);
            stres=stpat.exec(txt);
            console.log(res[0]);
            console.log(gres[0]);
            console.log(stres[0]);
            pat.lastIndex=1;
            gpat.lastIndex=1;
            stpat.lastIndex=1;
            res=pat.exec(txt);
            gres=gpat.exec(txt);
            stres=stpat.exec(txt);
            console.log(res[0]);
            console.log(gres[0]);
            //console.log(stres[0]);/throws error sticky stores index of next charcter
            //sticky property
            console.log(stpat.sticky);
            //duplicating Regular Expression
            pat=/ab/i;
            let pat2=new RegExp(pat,'g');//flags can be overridden in es6
            console.log(pat.test('AB'));
            console.log(pat2.test('AB'));
            //flag property
            console.log('source of Regex: ',pat2.source);
            console.log('flags of Regex: ',pat2.flags);
            //template literals
            txt=`I'm IronMan!`;
            console.log('Type of ',txt,' is ',typeof txt);
            //multiline string without template literals
            txt="string has \n two lines";
            console.log(txt);
            let strarr=['string has','two lines'];
            console.log(strarr.join('\n'));
            //multiline string using template literals
            txt=`Avengers 
Assemble`;
            console.log(txt);
            //all whitespace insdie `` are part of string
            txt=`'Wakanda
            Forever'`;
            console.log('length of ',txt,' is ',txt.length);
            //trim method
            txt=`
I'M
Groot!`;
            console.log(txt.trim());// trim removes the emptyline before the string 
            //substitution/String interpolation
            let tempName='IronMan';
            console.log(`I'm ${tempName}`);
            //substitute template literal inside another template literal
            console.log(`I'm Inevitable ${`and I'm ${tempName}`}`);
            let p1='Son of krypton';
            let p2='Bat of Gotham'
            txt=tag`The Battle between ${p1} and ${p2}.`;
            console.log(txt);
            function tag(strings,value){
                return 'Somethings';
            }
            txt=prints`The Battle between ${p1} and ${p2}.`;
            function prints(string,...values){
                let out='';
                for(let i=0;i<values.length;i++){
                    console.log(string[i],values[i]);
                    out+=string[i];
                    out+=values[i];
                }
                out+=string[string.length-1];
                return out;
            }
            console.log(txt);
            //using raw values in template literals
            txt=`Avengers\nAssemble`;
            console.log(txt);
            txt=String.raw`Avengers\nAssemble`;//\n is return as raw form of \\n
            console.log(txt);