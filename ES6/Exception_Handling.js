function exceptionHandling(){
    //try-catch Statement
    function samp(val){
        try{
            console.log('Value: ',x);//throws referrence error
            let x=val;
        }
        catch({name,message}){
            console.log('Error Name: ',name);
            console.log('Error Message: ',message);
        }
    }
    samp(5);
    //modifying code at catch block
    function samp1(val){
        try{
            console.log('Value: ',x);//throws referrence error
            let x=val;
        }
        catch(e){
            console.log('Value: ',val);
        }
    }
    samp1(7);
    //throw user-defined Exception as String
    function checkPassword(pas){
        try{
            if(pas.length<8)
                throw `Password '${pas}' is too short`;
            console.log(`'${pas}' is a Valid Password!!`);
        }
        catch(e){
            console.log('Error: ',e);
        }
    }
    checkPassword('Incorrect');//works fine
    checkPassword('Groot');//throws error because its length<8
    //throws user Exception as Error Object
    function checkPassword1(pas){
        try{
            if(pas.length<8)
                throw new Error(`Password '${pas}' is too short`);
            console.log(`'${pas}' is a Valid Password!!`);
        }
        catch(e){
            console.log('Error: ',e.message);
        }
    }
    checkPassword1('Incorrect');//works fine
    checkPassword1('Groot');
    //finally statement
    function samp2(val){
        try{
            console.log('Value: ',x);//throws referrence error
            let x=val;
        }
        catch({name,message}){
            console.log('Error Name: ',name);
            console.log('Error Message: ',message);
        }
        finally{
            console.log('Value: ',val);
        }
    }
    samp2(5);
}
exceptionHandling();