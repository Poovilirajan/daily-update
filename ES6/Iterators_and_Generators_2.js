function spreadOperatorAndNonArrayIterables(){
    //converting map into array using spread operator
    let map=new Map([['name','Steve'],['age',21]]);
    let map2=new Map([['id',1],['val',33]]);
    let mapArray=[...map,...map2];;
    console.log(mapArray);
    //string into array
    let str='Wakanda';
    let str2='Forever';
    let strArray=[...str,...str2,'!'];
    console.log(strArray);
    //Nodelist into array
    let headers=document.getElementsByTagName('h1');
    let headArray=[...headers];
    console.log(headArray);
}
function advancedIteratorFunctionality(){
    //Pass arguments to iterators
    function *gen(){
        yield 1;
        let temp=yield 3;
        let temp2=yield temp;
        yield temp2;
    }
    let iterate=gen();
    console.log(iterate.next());
    console.log(iterate.next(5));//5 is not lost because no variable was there in last call() to store the value
    console.log(iterate.next(4));
    console.log(iterate.next());//undefined because no value is passed here so temp2 is assigned with undefined but the done is false
    //throwing error in iterators
    function *gen1(){
        let temp;
        try{
            temp=yield 7;
            temp=yield temp+1;//this statement throws an error
        }
        catch(e){
            temp=1;
        }
        yield temp+2;
    }
    let iterate1=gen1();
    console.log(iterate1.next());
    console.log(iterate1.next(4));
    console.log(iterate1.throw(new Error('Haha!')));
    console.log(iterate1.next());
    //generator return statements
    function *gen2(){
        yield 1;
        return 3;
        yield 5;//these yield statements are unreachable
        yield 7;
    }
    let iterate2=gen2();
    console.log(iterate2.next());
    console.log(iterate2.next());
    console.log(iterate2.next());
    console.log(iterate2.next());
    function *oddGen(){
        yield 1;
        yield 3;
        yield 5;
        yield 7;
    }
    function *evenGen(){
        yield 2;
        yield 4;
        yield 6;
        yield 8;
    }
    function *numberGen(){
        yield *oddGen();
        yield *evenGen();
    }
    let number=numberGen();
    console.log(number.next());
    console.log(number.next());
    console.log(number.next());
    console.log(number.next());
    console.log(number.next());
    console.log(number.next());
    console.log(number.next());
    console.log(number.next());
    console.log(number.next());
    //Delegating generator using return
    function *strGen(){
        yield 'Stark';
        yield 'Tony';
        return 'Steve';//this value is not displayed when next() called
    }
    function *charGen(vals){
        for(v of vals)
            yield v;
    }
    let array=[3,7];
    function *nameGen(){
        temp=yield *strGen();//returned value of strGen() is stored in tempS
        yield *charGen(temp);
        yield *'Rogers';//this statement uses default string iterator
        yield *array;//it uses default array iterator
    }
    let names=nameGen();
    console.log(names.next());
    console.log(names.next());
    console.log(names.next());
    console.log(names.next());
    console.log(names.next());
    console.log(names.next());
    console.log(names.next());
    console.log(names.next());
    console.log(names.next());
    console.log(names.next());
    console.log(names.next());
    console.log(names.next());
    console.log(names.next());
    console.log(names.next());
    console.log(names.next());
    console.log(names.next());
}
//Asynchronous Task Running
function asynchronousTaskRunning(){
    //funtion to process generators
    function tasks(gen){
        let task=gen();//gets iterator
        let obj=task.next();
        while(!obj.done){//calling next() untill done is true
            console.log(obj.value);
            obj=task.next(obj.value);
        }
    }
    function *oddgen(){
        yield 1;
        yield 3;
        yield 5;
        yield 7;
    }
    function *numgen(){
        let temp=yield 0;
        temp=yield temp+1;
        temp=yield temp+1;
        temp=yield temp+1;
        temp=yield temp+1;
        temp=yield temp+1;
        temp=yield temp+1;
        temp=yield temp+1;
        temp=yield temp+1;
        yield temp+1;
    }
    tasks(oddgen);
    tasks(numgen);
}
spreadOperatorAndNonArrayIterables();
advancedIteratorFunctionality();
asynchronousTaskRunning();