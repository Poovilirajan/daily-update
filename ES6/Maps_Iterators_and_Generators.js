function weakMaps(){
    //creating weak maps
    let wMap=new WeakMap();
    let obj1={id:1};
    wMap.set(obj1,'Steve');//it only accepts keys as objects(notnull)
    //wMap.set('name','Wayne');//throws error it doesn't support primitive types
    console.log(wMap.get(obj1));
    let obj2={id:2};
    wMap.set(obj2,obj2.id);
    console.log(wMap.get(obj2));
    console.log(wMap);
    obj2=null;
    console.log(wMap.get(obj2));//undefined because outer reference to the key is removed
    console.log(wMap);
    console.log(wMap.size);//undefined because size property is not available
    //weakmap initialization using array
    let obj3={id:3};
    let arrWMap=new WeakMap([[obj1,obj1.id],[obj3,obj3.id]]);
    console.log(arrWMap);
    console.log('Map contains Obj1: ',wMap.has(obj1));
    console.log('Obj1 contains : ',wMap.get(obj1));
    wMap.delete(obj1);
    console.log('Map contains Obj1: ',wMap.has(obj1));
    console.log('Obj1 contains : ',wMap.get(obj1));
}
//Iterators
function iteratorsInEs5(){
    function iterate(items){
        let i=0;
        return{
            next:function(){
                let done=i>=items.length;
                let val=items[i];
                i++;
                return{
                    done:done,
                    value:!done?val:undefined
                };
            }
        }
    }
    let iterator=iterate([1,3,5,7,9]);
    console.log(iterator.next());
    console.log(iterator.next());
    console.log(iterator.next());
    console.log(iterator.next());
    console.log(iterator.next());
    console.log(iterator.next());
}
//Generators
function generators(){
    function *simpleGen(items){
        for(let i=0;i<items.length;i++)
            yield items[i] ;
    }
    let iterator=simpleGen([0,2,4,6,8]);
    console.log(iterator.next());
    console.log(iterator.next());
    console.log(iterator.next());
    console.log(iterator.next());
    console.log(iterator.next());
    console.log(iterator.next());
    //generator function expressions
    let gen=function *(vals){
        for(let v=0;v<vals.length;v++)
            yield vals[v];
    }
    let iterate=gen([3,5,7]);
    console.log(iterate.next());
    //generator moject method
    let obj={
        *generate(lists){
            for(let l=0;l<lists.length;l++)
                yield lists[l];
        }
    };
    let ite=obj.generate([11,22,33]);
    console.log(ite.next());
}
function iterablesAndForOf(){
    //for-of loop
    let odds=[1,3,5,7];
    for(let o of odds)
        console.log(o);
    //accessing default iterator
    let iterator=odds[Symbol.iterator]();
    console.log(iterator.next());
    //checking iterables
    console.log(typeof[1,2,3][Symbol.iterator]==='function');
    console.log(typeof new WeakMap()[Symbol.iterator]==='function');
    //creating iterables
    let userObj={
        vals:['Wayne','Steve','Stark'],
        *[Symbol.iterator](){
            for(let v of this.vals)
                yield v;
        }
    };
    for(let o of userObj)
        console.log(o);

}
function builtInIterators(){
    //collection iterators
    let array=[3,7];
    let set=new Set([12,6]);
    let map=new Map([['name','Rogers'],['age',70]]);
    //entries iterator
    for(let a of array.entries())
        console.log(a);//displays index and value
    for(let s of set.entries())
        console.log(s);//displays value two times
    for(let m of map.entries())
        console.log(m);//displays keys and values
    //values iterator
    for(let a of array.values())
        console.log(a);//displays values
    for(let s of set.values())
        console.log(s);//displays values 
    for(let m of map.values())
        console.log(m);//displays values only
    //keys iterator
    for(let a of array.keys())
        console.log(a);//displays indices
    for(let s of set.keys())
        console.log(s);//displays values 
    for(let m of map.keys())
        console.log(m);//displays keys only
    //destructuring in for-of loop
    for(let [v] of map)
        console.log('value: ',v);
    for(let [i,v] of array.entries())
        console.log('index: ',i,' value: ',v);
    //string iterator
    let msg='Im Inevitable!';
    for(let char of msg)
        console.log(char);
    //NodeList iterators

    let headers=document.getElementsByTagName('h1');
    for(let h of headers)
        console.log(h.textContent);
}
weakMaps();
iteratorsInEs5();
generators();
iterablesAndForOf();
builtInIterators();