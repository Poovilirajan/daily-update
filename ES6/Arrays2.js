//Typed Arrays
function typedArrays(){
    //ArrayBuffer
    let sampleBuffer=new ArrayBuffer(15);
    //sampleBuffer[0]=5;
    //console.log(sampleBuffer[0]);
    console.log(sampleBuffer);
    console.log(sampleBuffer.byteLength);//retrieves the number of bytes in a buffer
    let temp=sampleBuffer.slice(2,10);//returns the arraybuffer instance
    console.log(temp);
    console.log(temp.byteLength);
    //manipulating arraybuffers with views
    let sampleView1=new DataView(temp);//create view over all bytes of buffer
    let sampleView2=new DataView(sampleBuffer,4,5)//create view from byte 4 to byte 8
    let sampleView3=new DataView(sampleBuffer,5)//create view from byte 5 to the end of the buffer
    //retrieving view information
    console.log(sampleView1.buffer);//returns the buffer attached to view
    console.log(sampleView2.buffer);
    console.log(sampleView2.byteOffset);//returns the byte where view start from
    console.log(sampleView3.byteOffset);
    console.log(sampleView1.byteOffset);
    console.log(sampleView1.byteLength);//returns the number of bytes under the view
    console.log(sampleView2.byteLength);
    //reading and writing data
    sampleView1.setInt8(0,7);//write data into the buffer
    sampleView1.setInt16(1,257);
    console.log(sampleView1.getInt8(0));//read data in buffer byte 0
    console.log(sampleView1.getInt8(1));//1
    console.log(sampleView1.getInt8(2));//1 because the value was written at byte 1,2 is written by int16
    sampleView3.setFloat32(0,5.5);
    console.log(sampleView3.getFloat32(0,));
    //creating type-specific views
    let tempBuffer=new ArrayBuffer(20);
    console.log(tempBuffer);
    let type1=new Int16Array(tempBuffer,2,8);
    console.log(type1.buffer);
    console.log(type1.byteOffset);
    console.log(type1.byteLength);
    let sampInts16=new Int16Array(7);
    sampInts16[0]=5;
    console.log(sampInts16);
    console.log(sampInts16.byteLength);
    console.log(sampInts16.length);
    let sampInts8=new Int8Array(0);
    sampInts8[0]=5;//unable to store data because its an empty buffer
    console.log(sampInts8);
    //element size
    let tempInts8=new Int8Array(10);
    console.log(tempInts8.BYTES_PER_ELEMENT);
    let tempInts32=new Int32Array(10);
    console.log(tempInts32.BYTES_PER_ELEMENT);
    //creating Typed Array using objects
    let tempInts16=new Int16Array(tempInts32);//passing typed array as argument
    console.log(tempInts32.byteLength);//40,contains memory for 10 elements of 4bytes
    console.log(tempInts16.byteLength);//20, contains memory for 10 elements of 2 bytes
    //copy array into typed array
    let array1=[1,3,5,7,9];
    tempInts8=new Int8Array(array1);
    console.log(tempInts8);
    array1=['steve','rogers'];
    tempInts8=new Int8Array(array1);//typedArray stores 0 for each invalid value 
    console.log(tempInts8);
}
//similarities between Typed and Regular arrays
function similaritiesInTypedAndNormalArray(){
    //both arrays using length property, numeric indices to access data
    let array1=[1,3,5,7,9];
    let ints8=new Int8Array(array1);
    console.log('Length of Array: ',array1.length);
    console.log('Length of Typed Array: ',ints8.length);
    console.log('element at index 1 of array: ',array1[1]);
    console.log('element at index 1 of Typed array: ',ints8[1]);
    //common methods
    let tempints8=new Int8Array([1,2,3,4,5,6,7,8,9]);
    let total=tempints8.reduce((s,v)=>s+v);
    console.log('sum of values in array: ',total);
    let birthYear=new Int16Array([1978,1995,1997,1999,2000]);
    let age=birthYear.map(v=>2021-v);
    console.log(age);
    console.log(age instanceof Int16Array);
    //Both arrays having same Iterators
    //using spread operator on Typed array
    ints8=new Int8Array([2,4,6,8,0]);
    array1=[...ints8];
    console.log(ints8);
    console.log(array1)
    console.log(array1 instanceof Array);//true because spread operator converts Typed array into regular array
    //for-of loop in Typed array
    for(let [i,v] of ints8.entries())
        console.log('index ',i,'value ',v);
    //of() and from() method
    let tempFloats32=Float32Array.of(3.5,7.9,1.3);
    let ints32=Int32Array.from(birthYear);
    console.log(tempFloats32);
    console.log(tempFloats32 instanceof Float32Array)
    console.log(ints32);
    console.log(ints32 instanceof Int32Array);
}
//Difference between Typed and Regular arrays
function differencesinTypedAndNormalArrays(){
    let ints8=new Int8Array(2);
    console.log(ints8 instanceof Array);//false because Typed Arrays Don't inherit from Array
    console.log(Array.isArray(ints8));//false
    //size of Typed array cannot be resized
    console.log(ints8);
    console.log('Array size',ints8.length);
    ints8[0]=1;
    ints8[1]=3;
    ints8[2]=5;//this statement is ignored
    console.log(ints8);
    console.log('Array size',ints8.length);//size remains same
    //Additional Methods in Typed Array
    //set() method
    let tempInts8=new Int8Array(7);
    tempInts8.set([1,1,3]);//store the values in index 0,1,2
    console.log(tempInts8);
    tempInts8.set([3,5,7,9],1)//store the values from index 1
    console.log(tempInts8);
    //tempInts8.set([11,13],-1);//negativ value in optional argument throws error
    //SubArray() Method
    let temp2=tempInts8.subarray();//temp2 is a clone of tempints8
    console.log(temp2);
    let temp3=tempInts8.subarray(1,5)//extracts value from index 1 to index 4 and returns a new Typed array
    console.log(temp3);
    let temp4=tempInts8.subarray(4);//extracts value from index 3 to array leng and returns new typed array
    console.log(temp4);
}
function typesOfInheritance(){
    //Single inheritance
    class vehicle{
        constructor(name){
            this.name=name;
        }
        display(){
            console.log('Name: ',this.name);
        }
    }
    class Bike extends vehicle{
        constructor(name,brand,wheels){
            super(name);
            this.brand=brand;
            this.wheels=wheels;
        }
        display(){
            super.display();
            console.log('Brand: ',this.brand);
            console.log('Wheels: ',this.wheels);
        }
    }
    let Mt15=new Bike('Mt15','YAMAHA',2);
    Mt15.display();
    //Multi-level Inheritance
    class Vehicle{
        constructor(name){
            this.name=name;
        }
        getName(){
            return this.name;
        }
    }
    class VehicleType extends Vehicle{
        constructor(name,type){
            super(name);
            this.type=type;
        }
        getType(){
            return this.type;
        }
    }
    class Car extends VehicleType{
        constructor(name,type,model){
            super(name,type);
            this.model=model;
        }
        displayInfo(){
            console.log('Name: ',super.getName());
            console.log('Type: ',super.getType());
            console.log('Model: ',this.model);
        }
    }
    let car1=new Car('Suzuki','petrol','Expresso');
    car1.displayInfo();
}
creatingArrays();
newMethodsOnAllArray();
typedArrays();
similaritiesInTypedAndNormalArray();
differencesinTypedAndNormalArrays();
typesOfInheritance();