//Destructuring
//retrieve object data without Destructuring
function destructuringIntro(){
    let person={name:'Steve',age:70};
    let temp1=person.name;
    let temp2=person.age;
    console.log(temp1,temp2);
}
//Object destructuring
function objectDestructuring(){
    let person={name:'Steve',age:70};
    let{name,age}=person;
    console.log(name,age);
    //Destructuring assignments
    let bmw={
        type:'car',
        wheels:4
    };
    type='bike';
    wheels=2;
    console.log(type,wheels);
    var checkBmw=(val)=>console.log(val===bmw);
    checkBmw({type,wheels}=bmw);//bmw object passed to the val parameter
    console.log(type,wheels);
    //default values
    let person1={name1:'Stark',age1:21};
    let {name1,age1,surname}=person1;
    console.log(name1,age1,surname);//surname is undefined
    ({name1,age1,surname=name1}=person1);
    console.log(name1,age1,surname);//surname is assigned to a default value
    //Destructuring using different local variable names
    let{type:vehicleType,wheels:vehicleWheels}=bmw;
    console.log('Type : ',vehicleType,' Wheels : ',vehicleWheels);
    //default values in different local variable name
    let{name:personName,age:personAge,surname:nickName=personName}=person;
    console.log(personName,personAge,nickName);
    //nested object destructuring
    let person2={name2:'Bruce',dob2:{date:'03/07/2000'}};
    let{dob2:{date:birthDate}}=person2;
    console.log(birthDate);
}
function arrayDestructuring(){
//Array Destructuring
    let odds=[1,3,5,7];
    let [fodd,sodd]=odds;
    console.log('First odd: ',fodd,'second odd: ',sodd);
    //you can omit the data u don't want
    let[,,,favodd]=odds;
    console.log('Favourite odd number: ',favodd);
    //array destructuring assignments
    let dishes=['shawarma','friedrice','BBQ','Briyani','GryllChicken'];
    let favDish='BBQ';
    console.log('FavouriteDish: ',favDish);
    [,,,favDish]=dishes;
    console.log('FavouriteDish: ',favDish);
    //swap values of variables without using additional variable
    let x=3,y=5,z=7;
    console.log('x: ',x,'y: ',y,'z: ',z);
    [x,y,z]=[y,z,x];
    console.log('x: ',x,'y: ',y,'z: ',z);
    //default values
    let evens=[,2];
    let[firev=0,secev,thiev]=evens;
    console.log(firev,secev,thiev);//firev=0 and thiev=undefined
    //nested Destructuring Array
    let countries=['France',['Italy','Vatican'],'UK'];
    let[country1,[,subcountry1]]=countries;
    console.log(country1,subcountry1);
    //rest items
    let [singleodd,...remainingodds]=odds;
    console.log(singleodd);
    console.log(remainingodds);
    //rest items are used to clone array
    let clone1=dishes.concat();//cloning using concat()
    console.log('cloning without rest items using concat: ',clone1);
    let [...clone2]=dishes;//cloning using rest items
    console.log('cloning using rest items : ',clone1);
}
//Mixed destructuring
function mixedDestructuring(){
    let person3={name3:'Wayne',age3:21,
        dob:{date3:'03/07/2000',day3:'Teusday'},
        qualification:[10,12,'B.sc']
    };
    let person2={name2:'Bruce',dob2:{date:'03/07/2000'}};
    let{name3,dob:{date3},qualification:[,,cqual]}=person3;
    console.log('Name: ',name3,'\nDOB: ',date3,'\nCurrent Qualification: ',cqual);
}
function destructuredParameters(){
//Destructured parameters
    function salute(name,{gender,age}={}){//empty object{} helpful while call the function without object
        let sal='';
        if(gender=='male')
            sal='Mr.';
        else if(gender=='female')
            sal='Ms.'
        console.log('Name: ',sal,name);
        if(age)
            console.log('Age: ',age);
    }
    salute('Wayne',{gender:'male',age:22});
    salute('Steve',{age:21});//single argument
    salute('Tony');//Without arguments
}
//merge multiple array
function multipleArrayObjectDestructuring(){
    let person3={name3:'Wayne',age3:21,
        dob:{date3:'03/07/2000',day3:'Teusday'},
        qualification:[10,12,'B.sc']
    };
    let person={name:'Steve',age:70};
    let person2={name2:'Bruce',dob2:{date:'03/07/2000'}};
    let dishes=['shawarma','friedrice','BBQ','Briyani','GryllChicken'];
    let odds=[1,3,5,7];
    let [...clone2]=dishes;
    clone2=[...clone2,...dishes,...odds];
    console.log(clone2);
    //merge multiple objects
    person2={...person2,...person,...person3};
    let{person3:{name3:perName},person:{name:pername1}}={person3,person};
    console.log(person2);
    console.log(perName,pername1);
}
destructuringIntro();
objectDestructuring();
arrayDestructuring();
mixedDestructuring();
destructuredParameters();
multipleArrayObjectDestructuring();