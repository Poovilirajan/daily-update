//Expanded object functionalities
function objectLiteralSyntaxExtension(){
    //property initializer shorthand
    let fname='Steve';
    let lname='Rogers';
    let obj1={
        fname,
        lname
    };
    console.log(obj1);
    //concise methods
    obj1={
        fname,
        lname,
        getName(){
            return fname+' '+lname;
        }
    }
    console.log('Name :',obj1.getName());
    //computed property names
    let temp1='fname';
    let temp2='lname';
    obj1={
        [temp1]:'Tony',
        [temp2]:'Stark'
    }
    console.log(`Name: ${obj1.fname} ${obj1.lname}`);
}
//new methods
function newMethods(){
    //Object.is() method
    console.log(-0===+0);//true
    console.log(Object.is(-0,+0));//false
    console.log(NaN===NaN);//false
    console.log(Object.is(NaN,NaN));//true
    let obj1={
        fname:'Wayne'
    };
    let obj2={
        lname:'Bruce'
    };
    console.log(Object.is(obj1,obj2));//false
    obj1={
        fname:'Wayne'
    };
    obj2={
        fname:'Wayne'
    };
    console.log(Object.is(obj1,obj2));//false
    class User{
        constructor(name){
            this.name=name;
        }
    }
    let user1=new User('Steve');
    let user2=new User('Steve');
    console.log(Object.is(user1,user2));//even having same value its false
    //Object.assign() method
    obj1={
        fname:'Tony',
        lname:'Stark'
    };
    obj2={
        age:70
    }
    let user={};
    Object.assign(user,obj1,obj2);
    console.log(user);
    //same property will be overwritten
    obj2={
        lname:'sparks',
        age:70
    }
    Object.assign(user,obj1,obj2);//lname of obj1 is overwritten by lname of obj2
    console.log(user);
    //working with accessor properties
    obj1={
        fname:'Tony',
        lname:'Stark',
        get name(){
            return this.fname+' '+this.lname;
        }
    };
    obj2={};
    Object.assign(obj2,obj1);
    obj2=Object.getOwnPropertyDescriptor(obj2,'name');//returned value of name() assigned to value property of obj2 
    console.log(obj2);
    console.log(obj2.value);
    console.log(obj2.get);//accessor not available
}
//duplicate Object Properties and Own Property Enumeration Order
function duplicateObjectProperties(){
    'use Strict';
    let user1={
        fname:'Steve',
        lname:'Rogers',
        age:20,
        age:21,
        age:22,
        age:24,
        age:41,
        age:70
    }
    console.log(user1);
    console.log(user1.age);//display 70 because it assigned at the last occurance of the age property
    //Own Property Enumeration order
    let obj1={
        5:'five',
        1:'one',
        3:'three',
        2:'two',
        4:'four'
    }
    console.log(obj1);//numberic keys are ordered in ascending order
    obj1={
        e:'e',
        c:'c',
        b:'b',
        d:'d'
    }
    obj1.a='a';
    console.log(obj1);//string keys are in the order which they are added
}
//more powerful prototypes
function morePowerfulPrototypes(){
    //Changing object's prototype
    let cat={
        makeSound(){
            console.log('meow meow');
        }
    }
    let dog={
        makeSound(){
            console.log('wow wow');
        }
    }
    let animal=Object.create(dog);
    console.log(animal.makeSound());
    console.log(Object.getPrototypeOf(animal));
    Object.setPrototypeOf(animal,cat);//animal prototype is change from dog to cat
    console.log(animal.makeSound());
    console.log(Object.getPrototypeOf(animal));
    //easy prototypt access with super references
    cat={
        makeSound(){
            return 'meow meow';
        }
    }
    dog={
        makeSound(){
            return 'wow wow';
        }
    }
    animal={
        makeSound(){
            return super.makeSound();
        }
    }
    console.log(Object.getPrototypeOf(animal));
    //console.log(animal.makeSound());
    Object.setPrototypeOf(animal,dog);
    console.log(animal.makeSound());
    console.log(Object.getPrototypeOf(animal));
    //multilevel inheritance in object
    let baby=Object.create(animal);
    console.log(baby.makeSound());
    console.log(Object.getPrototypeOf(baby)===animal);
    console.log(Object.getPrototypeOf(animal)==dog);
}
objectLiteralSyntaxExtension();
newMethods();
duplicateObjectProperties();
morePowerfulPrototypes();