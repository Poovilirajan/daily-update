//var declaration
        function getValue(cond){
            if(cond){
                var val=5;//this declaration hoisted to top.
                return val;
            }
            else{
                return val;//here val1 is accessible.
            }
        }
        console.log("Returns value "+getValue(true));
        console.log("Returns undefined "+getValue(false));
        //let Declaration (block level Declarations)
        function getValue1(cond){
            if(cond){
                let val1=5;//this declaration doesn't hoisted to top.
                return val1;
            }
            else{
                return val1;//here val1 is not accessible.
            }
        }
        console.log("Returns value : "+getValue1(true));
        //console.log("produce error : "+getValue1(false));//produce error
        //No Redeclarations
        {
            var demo=30;
            //let demo=40;//syntax error
        }
        var demo1=30;
        {
            let demo1=40;//doesn't produce error
        }
        //const declaration
        const num=10;
        //const num1;//when declared it must be initialized unless throws error 
        //declaring objects with const
        const person={name:'wayne'};
        person.name='stark'//bounded value can changed.
        //person={name:'stark'};//change binded value throws TypeError.
        //Temporal dead zone
        if(true){
            //console.log(typeof val2);//reference error
            let val2=5;// this declaration was in Temporal Dead Zone
        }
        //Block binding in loop
        for(var i=0;i<10;i++){}
        console.log('i also accessible here',i);//i is hoisted to the top
        for(let i1=0;i1<10;i1++){
            
        }
        //console.log('i1 not accessible here',i1);//reference error
        //Functions in loop
        var funcs=[];
        for(var index=0;index<10;index++)
            funcs.push(()=>console.log(index));
        funcs.forEach(function(func){
            func();//prints 10 ten times because all functions referencing same variable
        })
        //to avoid the same reference use let declaration
        var funcs1=[];
        for(let index1=0;index1<10;index1++)
            funcs1.push(()=>console.log(index1));//each functions have own cop of index1
        funcs1.forEach(function(f){
           f(); //it prints 0-9
        });
        //const declaration in loop
        //for(const ind=0;ind<10;ind++)
          //  console.log(ind);   //throws Typeerror after one iteration
        //const in for-in loop will run without error
        for(const func1 of funcs1)//it creates new binding for each iteration
            func1();
        //Global Block Bindings
        var sample='I\'m IronMan!';
        console.log(window.sample);//sample property added to the global object
        window.sample='Wakanda forever!';
        console.log(window.sample);
        let samp='Avengers Assemble!';
        console.log(samp);//using let no property is added to the global object 
        console.log(window.samp);//prints undefined
        
        const samp1='Dormamo I come to bargain!'
        console.log(samp1);//using const no property is added to the global object
        console.log(window.samp1);