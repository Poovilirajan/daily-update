//Promise Basics
function promiseBasics(){
    //creating unsettled promises
    let f=require('fs');
    function read(file){
        return new Promise((res,rej)=>{
            f.readFile(file,'utf8',(err,data)=>{
                if(err){
                   rej(err);
                }
                else
                    res(data);
            });
        });
    }
    let promise=read('mybitbucket.txt');
    promise.then((data)=>{
        console.log(data);
    },
    (e)=>{
        console.log(e);
    });
    console.log('Check');//executed before the above statement
    let temp=new Promise((res,rej)=>{
        console.log('executor');//executed immediately
        res();
    });
    temp.then(()=>{
        console.log('Resolve');//executed Asynchronously
    });
    console.log('Execution check');
    //Creating settled promises
    //Promise.resolve() method
    let setPromise=Promise.resolve('Steve');//no job scheduling happens
    setPromise.then((d)=>{
        console.log(d);
    });
    //Promise.reject() method
    setPromise=Promise.reject(new Error('Custom Error'));
    setPromise.then(null,(e)=>{
       console.log(e.message); 
    });
    //Non-promise thenables
    let obj1={
        then(res,rsj){
            res('Rogers');
        }
    }
    let promise1=Promise.resolve(obj1);
    promise1.then((d)=>{
        console.log(d);
    });
    let obj2={
        then(res,rej){
            rej(new Error('Custom Error'));
        }
    }
    let promise2=Promise.reject(obj2);
    promise2.then(null,(e)=>{
       console.log(e);
    });
    console.log('Thenable check');
    //Executor errors
    let promise3=new Promise((res,rej)=>{
        let num=2/0;
        res(num);
    });
    promise3.then((d)=>{
        console.log(d);
    },
    (e)=>{
        console.log(e);
    });
    //use try catch on Executors errors
    let promise4=new Promise((res,rej)=>{
        try{
            let num=2/0;
            res(num);
        }
        catch(e){
            rej(e);
        }
    });
    promise4.then((d)=>{
        console.log(d);
    },
    (e)=>{
        console.log(e);
    });
}
function globalPromiseRejectionHandling(){
    //Node.js Rejection Handling
    //using unhandledRejection handler
    let rejected1,rejected2;
    process.on('unhandledRejection',(reason,object)=>{
        console.log(reason);
        console.log(object);
    });
    rejected1=Promise.reject(2/0);
    //using rejectionHandled handler
    process.on('rejectionHandled',(object)=>{
        console.log(object);
    });
    rejected2=Promise.reject(new Error('custom error'));
    setTimeout(()=>{
        rejected2.then(null,(e)=>{
            console.log(e.message);
        });
    },2000);
}
function browserRejectionHandling(){
    window.onunhandledrejection=(e)=>{
        console.log(e.type);
        console.log(e.promise);
        console.log(e.reason);
    }
    window.onrejectionhandled=(e)=>{
        console.log(e.type);
        console.log(e.promise);
        console.log(e.reason);
    }
    let rejected=Promise.reject(2/0);
    setTimeout(()=>{
        rejected.then(null,(e)=>{
            console.log(e.message);
        });
    },2000);
}
//Chaining Promises
function chainingPromises(){
    //then() and catch() method creates another promise
    let promise1=Promise.resolve('Stark');
    promise1.then((d)=>{
        console.log('Name: ',d);
    }).then((d)=>{
        console.log('This is the called after the first resolved!');
    });
    let promise2=Promise.reject(new Error('Custom Error'));
    promise2.then(null,(e)=>{
        console.log('Error: ',e.message);//first promise handled by rejection handler
    }).then((e)=>{//second promise is handled by resolved handler
        console.log('This is the called after the first reject!');
    });
    let promise3=Promise.resolve('Steve');
    let p1=promise3.then((d)=>{
        console.log('Name: ',d);
    });
    p1.then((d)=>{
        console.log('This is the called after the first resolved!');
    });
    let promise4=Promise.reject(2/0);
    let p2=promise4.then(null,(e)=>{
        console.log('Error: ',e);
    });
    p2.then((e)=>{
        console.log('This is the called after the first reject!');
    });
    //catching errors
    let promise5=Promise.resolve(7);
    promise5.then((d)=>{
        throw new Error(d/0);
    }).then(null,(e)=>{
        console.log('Error1: ',e.message);
    });
    let promise6=Promise.reject(7/0);
    promise6.then(null,(e)=>{
        console.log('First Error: ',e);
        throw new Error('User Error');
    }).catch((e)=>{
        console.log('Second Error: ',e.message);
    });
    //Returning values in promise chains
    let promise7=Promise.resolve('Groot!');
    promise7.then((d)=>{
        console.log('String: ',d);
        return d;
    }).then((d)=>{
        console.log('Length of a string: ',d.length);
    });
    let promise8=Promise.reject(new Error('Sample Error!'));
    promise8.catch((e)=>{
        console.log('Error Message: ',e.message);
        return e.message;
    }).then((d)=>{
        console.log('Length of Error Message:',d.length);
    });
    //Returning Promises in Promise chains
    let promise9=Promise.resolve('Tony');
    let promise10=Promise.reject(new Error('Promise Error!'));
    promise9.then((d)=>{
        console.log(d);
        return promise10;//this promise is handled by rejection handler if rejection handler is absent this rejected promise is ignored
    }).catch((e)=>{
        console.log(e.message);
    });
    let promise11=Promise.reject(5/0);
    let promise12=Promise.resolve('Bruce');
    promise11.catch((e)=>{
        console.log('Promise Error: ',e);
        return promise12;
    }).then((d)=>{
        console.log('value: ',d); 
    });
    let promise13=Promise.resolve('Rogers');
    promise13.then((d)=>{
        console.log('value1: ',d);
        let temp=Promise.resolve(d);
        return temp;
    }).then((d)=>{
        console.log('Length of value1: ',d.length);
    });
}
//Responding to Multiple Promises
function respondingToMultiplePromises(){
    //Promise.all() method
    let promise1=Promise.resolve('Steve');
    let promise2=Promise.resolve('Rogers');
    let promise3=Promise.resolve('Tony');
    let promise4=Promise.resolve('Stark');
    let promise5=Promise.all([promise1,promise2,promise3,promise4]);
    promise5.then((d)=>{//all promises are fulfilled so it handled by fulfillment handler
    console.log(d);//value containing resolved values of all promises
    });
    let promise6=Promise.reject(new Error('First Error'));
    let promise7=Promise.resolve('Bruce');
    let promise8=Promise.reject(new Error('Second Error'));
    let promise9=Promise.all([promise6,promise7,promise8]);
    promise9.then((d)=>{//promise rejected because some of its promises are rejected so it handled by Rejection handler
        console.log(d);
    },(e)=>{
        console.log(e);
    });
    //Promise.race() method
    let promise10=new Promise((res,rej)=>{
        rej(new Error('Race Error!'));
    });
    let promise11=Promise.resolve('Rody');
    let promise12=Promise.race([promise10,promise11]);//promise is rejected because the first settled promise is rejeceted
    promise12.then((d)=>{
        console.log(d);
    },(e)=>{
        console.log(e);
    });
    promise13=Promise.race([promise11,promise10]);//promise is fulfilled because the first settled promise is fulfilled
    promise13.then((d)=>{
        console.log(d);
    },(e)=>{
        console.log(e);
    });
}
//Inheriting from Promises
function inheritingFromPromises(){
    class DemoPromise extends Promise{
        constructor(fun){
            super(fun);
        }
        settled(res,rej){
            super.then(res,rej);
        }
        rejected(rej){
            super.catch(rej);
        }
    }
    let tempPromise=new DemoPromise((res,rej)=>{
        res('Bruce');
    });
    tempPromise.settled((d)=>{
        console.log(d);
    });
    //all static methods of promise is also available DemoPromise class
    let promise1=DemoPromise.resolve('Steve');
    let promise2=DemoPromise.resolve('Rogers');
    let promise3=DemoPromise.all([promise1,promise2]);
    promise3.then((d)=>{
    console.log(d);
    });
    let promise4=DemoPromise.reject(new Error('First Error'));
    let promise5=DemoPromise.all([promise4,promise2]);
    promise5.then((d)=>{
        console.log(d);
    },(e)=>{
        console.log(e);
    });
    let promise6=DemoPromise.race([promise2,promise4]);
    promise6.then((d)=>{
        console.log(d);
    },(e)=>{
        console.log(e);
    });
}
promiseBasics();
globalPromiseRejectionHandling();
browserRejectionHandling();
chainingPromises();
respondingToMultiplePromises();
inheritingFromPromises();