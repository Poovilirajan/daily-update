//Node.js
//modules
const{reverse}=require("./hello");
let argument=process.argv[2];
console.log(reverse(argument));
let{readFile}=require('fs');
//File system module
//write file
let{writeFile}=require('fs');
writeFile('mybitbucket1.txt','file writted',err=>{
	if(err)
		console.log('failed to write :',err);
	else
		console.log('File written!');
});
readFile('mybitbucket1.txt','utf8',(error,text)=>{
	if(error)
		throw error;
	console.log('file contains',text);
});
//file system using buffer
readFile('mybitbucket.txt',(error,buffer)=>{
	if(error)
		throw error;
	console.log('file contained :',buffer.length,'bytes the first byte is: ',buffer[0]);
});
const{readFileSync}=require('fs');
console.log('The file contains : ',readFileSync('mybitbucket1.txt','utf8'));
let msg='hello world';
console.log(msg);

exports.reverse=function(str){
	return Array.from(str).reverse().join('');
};

const{createServer}=require('http');
let server=createServer((request,response)=>{
	response.writeHead(200,{'Content-Type':'text/html'});
	response.write(`<h1>Hello</h1>
	<p>You asked for <code>${request.url}</code><p>`);
	response.end();
});
server.listen(8000);
console.log('Listening! (port 8000)');