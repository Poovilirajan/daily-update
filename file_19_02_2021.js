//Sample JS Program
let count=1,total=0;
while(count<=10){
    total=total+count;
    count++;
}
console.log(total);
//sample function
function fact(n){
    if(n==0)
        return 1;
    else
        return fact(n-1)*n;
}
console.log(fact(7));
//INFINITY
console.log(2/0);
//NaN
var a;//a is an undefinrd value 
console.log(a+5);

//String with Double quote
a="Man of Steel";
//String with single quote
a='Son of krypton';
//String with Backticks
a=`Bat of Gotham`;

//string with quotes
a='I\'m IronMan'
console.log(a);

//String concatenation using +operator
console.log('Aven'+'gers '+'Assem'+'ble!');
//String concatenation using method
console.log('Wakanda'.concat(' Forever'));

//String interpolation
console.log(`2 * 2 = ${2*2}`)

//unary typeof operator
console.log(typeof 2.5);
console.log(typeof 'text');

//String comparision uppercase alphabet < lowercase alphabet
console.log('a'>'A');

//NaN is not equal to itself
console.log(NaN==NaN);

//Logical operators
//AND &&
console.log(true && false);
//OR ||
console.log(true || false);
//NOT !
console.log(!true);

//TERNARY OPERATORS
a=5;
let b=10;
console.log(a>b?a:b);

//TYPE CONVERSION
console.log(8*null);//null is converted into 0
console.log('8'-1);//'8' string is converted into number 8
console.log('7'+0);//number 0 is converted into string'0'
console.log(false==0);//false is converted into number 0

console.log(null==undefined)//true because if null or undefined placed both side of the operator

//to prevent type conversion use === operator
console.log(true===1);//returns false beacuse === operator also compares the type of value

//short circuiting of logical operators
console.log(null||'text');//prints text
console.log('user'||'text');//prints user

console.log(null&&'text');//prints null
console.log('user'&&'text');//prints text

//binding
let five=5;

//conditionals
if(five==5){
    console.log("The number is ",five);
}

if(five==5)
    console.log("The number is ",five);
else
    console.log("the number is not five");

if(five<10)
    console.log("The number is small");
else if(five<100)
    console.log("the number is medium");
else
    console.log("the number is large");

//While loop
let n=0;
while(n<=12){
    console.log(n);
    n=n+2;
}
//Do...While loop
/*let name='';
do{
    name=prompt('Enter name');
}while(!name);
console.log(name);*/

//for loop
let ans=1;
for(let count=0;count<10;count++)
    ans=ans*2;
console.log("2^10 : ",ans);

//breaking out of loop
//Break
for(let index=0;;index+=1){
    console.log(index);
    if(index==4)
        break;
}
//continue
for(let index=0;index<10;index+=1){
    if(index==4)
        continue;
    console.log(index);
}

//Switch
let day=3;
switch(day){
    case 1: console.log('Sunday');
        break;
    case 2: console.log('Monday');
        break;
    case 3: console.log('Tuesday');
        break;
    case 4: console.log('Wednesday');
        break;
    case 5: console.log('Thursday');
        break;
    case 6: console.log('Friday');
        break;
    case 7: console.log('Saturday');
        break;
    default: console.log('Invalid Choice');
        break;
}