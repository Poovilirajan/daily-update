import React from 'react';
//rendering multiple items through Lists
export function OddList(props){
    let items=props.list.map(item =><li>{item}</li>);
    return(<div><h3>OddNumbers</h3><ul>{items}</ul></div>);
}
//adding keys to List elements
export function EvenList(props){
    let items=props.list.map(item=><li key={item.toString()}>{item}</li>);
    return(<div><h3>Even Numbers</h3><ul>{items}</ul></div>);
}
//use index as key to list elements
export function Names(props){
    let items=props.list.map((item,index)=><li key={index}>{item}</li>);
    return(<div><h3>Names</h3><ul>{items}</ul></div>);
}
//elements inside map() only need keys
function ListItem(props){
    return <li>{props.value}</li>;
} 
export function Numbers(props){
    let items=props.list.map(item=><ListItem key={item.toString()} value={item} />);
    return(<div><h3>Numbers</h3><ul>{items}</ul></div>);
}
//using map() inside JSX
export function OddList1(props){
    return(<div><h3>Odds</h3><ul>{props.list.map(item=><li key={item.toString()}>{item}</li>)}</ul></div>);
}
//Composition using children prop
function Container(props){
    return(<div id='container'>{props.children}</div>);
}
export function Dialog(props){
    return(<Container>
        <h2>Weclcome</h2>
        <h3>{props.name&& 'User : '+props.name}</h3>
    </Container>)
}
//composition without using children prop
function Container1(props){
    return(<div id='container1'>
        {props.first}
        {props.second}
    </div>);
}
export function Dialog2(props){
    let first=<h2>Welcome</h2>;
    let second=<h3>{props.name && 'User: '+props.name}</h3>;
    return(<Container1 first={first} second={second}/>);
}
let temp=createEl('div');
ReactDOM.render(<OddList list={[1,3,5,7,9]}/>,temp);
temp=createEl('div');
ReactDOM.render(<EvenList list={[0,2,4,6,8]}/>,temp)
temp=createEl('div');
ReactDOM.render(<Names list={['Steve','Stark','Wayne']}/>,temp);
temp=createEl('div');
ReactDOM.render(<Numbers list={[1,3,4,5,7,6]}/>,temp);
temp=createEl('div');
ReactDOM.render(<OddList1 list={[9,7,5,3,1]}/>,temp);
temp=createEl('div');
ReactDOM.render(<Dialog name='Steve'/>,temp);
temp=createEl('div');
ReactDOM.render(<Dialog2 name='Stark'/>,temp);