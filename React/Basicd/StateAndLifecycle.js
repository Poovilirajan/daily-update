import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
const container=document.getElementById('root')
function createEl(elem){
    let temp=document.createElement(elem);
    container.appendChild(temp);
    return temp;
}
//adding local state to Component
class Clock extends React.Component{
    constructor(prop){
        super(prop);
        this.state={date: new Date()};
    }
    render(){
        return(
            <div>
            <h3>Time1 : </h3>
            <h3>{this.state.date.toLocaleTimeString()}      </h3>
            </div>
        );
    }
}
let temp=createEl('div');
ReactDOM.render(<Clock />,temp);
//adding lifecycle methods to Component
class Clock1 extends React.Component{
    constructor(props){
        super(props);
        this.state={date:new Date()};
    }
    render(){
        return(
            <div>
                <h3>Time2 : </h3>
                <h3>{this.state.date.toLocaleTimeString()}</h3>
            </div>
        );
    }
    componentDidMount(){
        setInterval(()=>this.count(),1000);
    }
    count(){
        this.setState({date:new Date()});
    }
}
temp=createEl('div');
ReactDOM.render(<Clock1/>,temp);
//Event Handilng
class Game extends React.Component{
    shoot(){
        alert('Goal!');
    }
    render(){
        return <button onClick={this.shoot}>Shoot Me</button>;
    }
}
temp=createEl('div');
ReactDOM.render(<Game/>,temp);
//return false not available for prevent default in Events
class Link extends React.Component{
    handle(e){
        console.log('link clicked');
        e.preventDefault();
        console.log('click event prevented from redirecting')
    }
    render(){
        return <a href='http://www.google.com' onClick={this.handle}>click to redirect!</a>
    }
}
temp=createEl('div');
ReactDOM.render(<Link/>,temp);
//use arrow functions to use this reference of component in handler function
class Button1 extends React.Component{
    handle=()=>{
        console.log(this);
    }
    render(){
        return <button onClick={this.handle}> Click to see my reference</button>
    }
}
temp=createEl('div');
ReactDOM.render(<Button1/>,temp);
//passing arguments to event handlers
class Name extends React.Component{
    constructor(props){
        super(props);
        this.name=props.name;
    }
    sayName(str){
        console.log('Name : ',str);
    }
    render(){
        return <button onClick={()=>this.sayName(this.name)}>Say Name</button>;
    }
}
temp=createEl('div');
ReactDOM.render(<Name name='Stark'/>,temp);