import React from 'react';
export default class TextArea extends React.Component{
    constructor(props){
        super(props);
        this.state={value:''};
        this.inputRef=React.createRef();
    }
    changeInput=()=>{
        console.log(this.state.value);
        this.setState({value:this.inputRef.current.value});
    }
    submitValue=ev=>{
        console.log('Essay : ',this.state.value);
        ev.preventDefault();
    }
    clearInput=ev=>{
        this.inputRef.current.value='';
        this.setState({value:''});
        ev.preventDefault();
    }
    render(){
        return(
            <form onSubmit={this.submitValue}>
                <textarea placeholder='Start typing an essay...' ref={this.inputRef} onChange={this.changeInput}/><br/>
                <input type='submit' value='submit'/>
                <button onClick={this.clearInput}>Clear</button>
            </form>
        );
    }
}
export default class MultipleInput extends React.Component{
    constructor(props){
        super(props);
        this.state={name:'',age:'',gender:''};
    }
    ChangeInput=({target})=>{
        this.setState({[target.name]:target.value});
    }
    submitInput=ev=>{
        console.log('Name: ',this.state.name);
        console.log('Age: ',this.state.age);
        console.log('Gender: ',this.state.gender);
        ev.preventDefault();
    }
    render(){
        return(
            <form onSubmit={this.submitInput}>
                <input placeholder='Username' name='name' onChange={this.ChangeInput}/><br/>
                <input placeholder='age' name='age' onChange={this.ChangeInput}/><br/>
                <label>Gender
                    <input type='radio' onChange={this.ChangeInput} name='gender' value='male'/>male
                    <input type='radio' onChange={this.ChangeInput} name='gender' value='female'/>female
                </label><br/>
                <input type='submit' value='submit'/>
            </form>
        );
    }
}
temp=createEl('div');
ReactDOM.render(<TextArea/>,temp);
temp=createEl('div');
ReactDOM.render(<MultipleInput/>,temp);
temp=createEl('div');