import React from 'react';
//Conditional endering
export class Greeting extends React.Component{
    constructor(props){
        super(props);
        this.props=props;
    }
    render(){
        if(this.props.gender==='male')
            return <h1>Welcome Mr.{this.props.name}</h1>
        else if(this.props.gender==='female')
            return <h1>Welcome Ms.{this.props.name}</h1>
    }
}
//store element in variable for conditional rendering
export class LoginControl extends React.Component{
    constructor(props){
        super(props);
        this.props=props;
        this.state={isLoggedIn:false};
    }
    logIn=()=>{
        this.setState({isLoggedIn:true});
    }
    logOut=()=>{
        this.setState({isLoggedIn:false});
    }
    render(){
        let button;
        if(this.state.isLoggedIn)
            button=<button onClick={this.logOut}>LogOut</button>;
        else
            button=<button onClick={this.logIn}>LogIn</button>;
        return button;
    }
}
//inline conditional rendering with &&(short circuiting)
export class Name extends React.Component{
    render(){
        return(<h1>Hi{this.props.name && <span>, My Name is {this.props.name}!</span>}</h1>);
    }
}
//inline conditional rendering with conditional operator
export class Status extends Name{
    render(){
        return <h1>The User is {this.props.name?this.props.name:'Guest'}.</h1>
    }
}
//preventing component from rendering with null
function Warning(props){
    if(props.warn)
        return <h3>Warning!</h3>;
    else
        return null;
}
export class Document extends React.Component{
    constructor(props){
        super(props);
        this.state={warning:false};
    }
    handle=()=>{
        this.setState(state =>({warning: !state.warning}));
    }
    render(){
        return(
            <div>
                <Warning warn={this.state.warning}/>
                <button onClick={this.handle}>{this.state.warning?'hide':'show'}</button>
            </div>
        );
    }
}
let temp=createEl('div');
ReactDOM.render(<Greeting gender='male' name='Stark'/>,temp);
temp=createEl('div');
ReactDOM.render(<LoginControl/>,temp);
temp=createEl('div');
ReactDOM.render(<Name name='Wayne'/>,temp);
temp=createEl('div');
ReactDOM.render(<Status name='Steve'/>,temp);
temp=createEl('div');
ReactDOM.render(<Document/>,temp);