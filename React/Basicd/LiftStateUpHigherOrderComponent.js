import React from 'react';
export default class Banner extends React.Component{
    constructor(props){
        super(props);
    }
    render(){
        return <h3>Value: {this.props.value}</h3>;
    }
}
export default class InputBox extends React.Component{
    constructor(props){
        super(props);
        this.textRef=React.createRef();
    }
    handleChange=()=>{
        this.props.changeInput(this.textRef.current.value);
    }
    render(){
        return(<>
            <input value={this.props.value} onChange={this.handleChange} ref={this.textRef}/>
        </>);
    }
}
export default class BannerContainer extends React.Component{
    constructor(props){
        super(props);
        this.state={inpValue:''};
    }
    handleChange=v=>{
        this.setState({inpValue:v});
    }
    render(){
        return(<div>
            <InputBox value={this.state.inpValue} changeInput={this.handleChange}/>
            <Banner value={this.state.inpValue}/>
        </div>);
    }
}
function Name(props){
    return <h3>{props.name}</h3>;
}
function hoc(Comp){
    return class extends React.Component{
        constructor(props){
            super(props);
            this.names=['Steve','Tony','Thor','Wayne'];
        }
        render(){
            return(<>
            {this.names.map(name=> <Comp name={name.toLocaleUpperCase()}/>)}
            </>);
        }
    }
}
export const NameContainer1=hoc(Name);
temp=createEl('div');
ReactDOM.render(<NameContainer1/>,temp);
temp=createEl('div');
ReactDOM.render(<BannerContainer/>,temp);