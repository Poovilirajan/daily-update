import React, { useRef } from 'react';
//creating ref
export class DemoComponent extends React.Component{
    constructor(props){
        super(props);
        this.demRef=React.createRef();
    }
    render(){
        return <div red={this.demRef}>Hello!</div>;
    }
}
//accessing ref
//In Dom element
export class TextBox extends React.Component{
    constructor(props){
        super(props);
        this.tRef=React.createRef()
        this.handle=this.handle.bind(this);
    }
    handle(){
        this.tRef.current.value='';
    }
    aFocus=()=>{
        this.tRef.current.focus();
    }
    render(){
        return(<div>
            <input type='text' ref={this.tRef} /><br/>
            <button onClick={this.handle}>clear</button>
        </div>);
    }
}
//In Class component
export class AutoFocusTextBox extends React.Component{
    constructor(props){
        super(props);
        this.ref=React.createRef();
    }
    render(){
        return <TextBox ref={this.ref}/>;
    }
    componentDidMount(){
        this.ref.current.aFocus();
    }
}
//using ref in function component
export function TextBox2(props){
    const tRef=useRef(null);
    function handle(){
        tRef.current.value='';
    }
    return(<div>
        <input type='text' ref={tRef}/><br/>
        <button onClick={handle}>clear</button> 
    </div>);
}
//callback refs
export class Textbox3 extends React.Component{
    constructor(props){
        super(props);
        this.tRef=null;
    }
    setRef=(element)=>{
        this.tRef=element;
    }
    handle=()=>{
        if(this.tRef)
            this.tRef.focus();
    }
    render(){
        return(
            <div>
                <input type='input' ref={this.setRef}/><br/>
                <button onClick={this.handle}>Focus</button>
            </div>
        )
    }
}
//React without JSX
export function Greet(props){
    return React.createElement('div',null,`Welcome ${props.name} !`);
}
//shorthand for React.createElement
export const reCreate=React.createElement;
//fragments
export function NameContainer(props){
    return(<React.Fragment>
        <Greet name='Tony'/>
        <Greet name='Bruce'/>
        <Greet name='Rogers'/>
    </React.Fragment>);
}
//shorthand syntax
export function Lists(props){
    return(<>
        <li>Steve</li>
        <li>Thor</li>
        <li>Tony</li>
    </>);
}
temp=createEl('div');
ReactDOM.render(<DemoComponent/>,temp);
temp = createEl('div');
ReactDOM.render(<TextBox/>,temp);
temp =createEl('div')
ReactDOM.render(<AutoFocusTextBox/>,temp);
temp=createEl('div');
ReactDOM.render(<TextBox2/>,temp);
temp=createEl('div');
ReactDOM.render(<Textbox3/>,temp);
temp=createEl('div');
ReactDOM.render(React.createElement(Greet,{name:'Steve'},null),temp);
temp=createEl('div');
ReactDOM.render(reCreate(Greet,{name:'Wayne'},null),temp);
temp=createEl('div');
ReactDOM.render(<NameContainer/>,temp);
temp=createEl('ul');
ReactDOM.render(<Lists />,temp);