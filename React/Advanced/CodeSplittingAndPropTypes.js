import PropTypes from 'prop-types';
import React from 'react';

//checking Component prop is an array
export default class List1 extends React.Component{

    constructor(props){
        super(props);
        if(props.names)
        this.items=props.names.map(name=><li key={name.toString()}>{name}</li>);
    }

    render(){
        return<ul>{this.items}</ul>;
    }

}

List1.propTypes={
    names:PropTypes.any.isRequired,
};
//validating props in function component
export default function SampComponent(props){
    return<h3>{props.value}</h3>;
}

SampComponent.propTypes={
    value:PropTypes.string
};
//default value for component props
export default class Welcome extends React.Component{
    render(){
        return<h3>{this.props.name}</h3>;
    }

}

Welcome.defaultProps={
    name:'Guest'
};
//dynamically import Component using React.lazy()
temp=createEl('div')
Element=React.lazy(()=>import('./Components/LiftingStateup/BannerContainer'));
ReactDOM.render(<Suspense fallback={<p>loading results!..</p>}><Element/></Suspense>,temp);
//Validating class component props using PropTypes
temp=createEl('div');
ReactDOM.render(<List1 names={['Steve']}/>,temp);
temp=createEl('div');
ReactDOM.render(<List1 />,temp);
//Validating function component props using PropTypes
temp=createEl('div');
ReactDOM.render(<SampComponent value={7}/>,temp);
temp=createEl('div');
ReactDOM.render(<SampComponent value='Seven'/>,temp);
//declaring default value for Component props
temp=createEl('div');
ReactDOM.render(<Welcome name={'Wayne'}/>,temp);
temp=createEl('div');
ReactDOM.render(<Welcome />,temp);