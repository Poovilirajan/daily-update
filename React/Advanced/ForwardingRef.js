import React from'react';
//forwarding ref to its child 
export default class DemoForm extends React.Component{
    constructor(props){
        super(props);
        this.state={value:''};
        this.textRef=React.createRef();
    }
    handleSubmit=(e)=>{
        console.log('Value: ',this.textRef.current.value);
        e.preventDefault();
    }
    handleChange=(value)=>{
        this.setState({value});
    }
    render(){
        return(
            <form onSubmit={(e)=>{this.handleSubmit(e)}}>
                <TextInput2 ref={this.textRef} change={this.handleChange}/>
                <input type='submit' value='submit'/>
            </form>
        );
    }
}
export const TextInput2= React.forwardRef((props,ref)=>{
    function handle(){
        props.change(ref.current.value);
    }
    return <input placeholder='Username' ref={ref} onChange={()=>handle()}/>;
});
//forwarding ref in higher order ccomponent
export function highForward(Comp){
    let temp=(props,ref)=>{
        function handle(){
            console.log('value: ',ref.current.value);
        }
        return<Comp ref={ref} onChange={()=>handle()} {...props}/>;
    }
    return React.forwardRef(temp);
}
export function DemoInp(props){
    return<input type='text' placeholder='Type something...'/>;
}
temp=createEl('div');
ReactDOM.render(<DemoForm/>,temp);
temp=createEl('div');
let Element=highForward(DemoInp);
let elementRef=React.createRef();
ReactDOM.render(<Element ref={elementRef}/>,temp);