const heading1=document.querySelector('h1');
heading1.textContent="I'm IronMan!"

//difference between var and let
var a;
console.log('value of a : '+a);
console.log('value of b : '+b);//variable hoisting
var b;

let x;
console.log('value of x : '+x);
//console.log('value of y : '+y);//reference error
let y;

console.log('Undefined + number : '+(a+2));
var n=null;
console.log('null with number : '+(n*7));

//variable scope

if(true){
    var a1=5;//local scope
}
console.log("a1 : "+a1);

if(true){
    let x1=5;//block scope
}
//console.log("x1 : "+x1);//reference error

var myvar='value1';
(function(){
    console.log(myvar);
    var myvar='funvalue';//variable hoisting inside a function
})();

//function hoisting
fun1();//function declaration

function fun1(){
    console.log('function hoisting works on declaration');
}

//fun2(); //function expression //not a function error
var fun2=function(){
    console.log('function hoisting works on expression');
}
//constants
const f=5;
//function f(){} // cause error 
//var f=7; cause error

const obj1={'id':5}
console.log("id of obj1 : "+obj1.id);
obj1.id=7;//constant objects are not protected 
console.log("id of obj1 : "+obj1.id);

const array1=['HTML','CSS'];
console.log(array1);
array1.push('JS');// constant arrays are not protected
console.log(array1);

//template literals
var sample=`This 
is a 
template\nliteral`;/*template literal can be multiline string and accepts escape characters like \n*/
console.log(sample);
//string interpolation
var name='Wayne'; var time='today';
sample=`Hi ${name}, How r u ${time}`;
console.log(sample);

//conditionals
let icecream='blackcurrent';
if(icecream==='blackcurrent')
    alert('I Love '+icecream+' ice cream');
else
    alert('Aww, i think the ice cream is '+icecream);
    
//below code used to add functionality to the page
let image1=document.querySelector('img');
image1.onclick=function(){
    let src=image1.getAttribute('src');
    if(src==='images/frontis-piece.jpg')
        image1.setAttribute('src','images/brass-telescope.png');
    else
        image1.setAttribute('src','images/frontis-piece.jpg');
};      //this block used change image when click the image displayed in the Html 

let button1=document.querySelector('button');
let header1=document.querySelector('h1');
button1.onclick=function(){
    if(!localStorage.getItem('user')){
        let user=prompt("Enter your name");
        localStorage.setItem('user',user);
        header1.textContent="Welcome "+user+"!";
    }
    else{
        let existinguser=localStorage.getItem('user');
        header1.textContent="Welcome "+existinguser+"!";
    }
}//this block used to add custom heading to the page using the current username 
